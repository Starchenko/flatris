FROM node:latest

RUN mkdir /app
WORKDIR /app
COPY package.json /app
RUN yarn install

COPY . /app

RUN yarn build
# RUN yarn test
CMD yarn start
EXPOSE 3000
